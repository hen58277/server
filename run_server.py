from os import abort
import cv2
from apscheduler.schedulers.background import BackgroundScheduler
from tensorflow.keras.applications.efficientnet import preprocess_input
from flask_cors import CORS, cross_origin
from tensorflow.keras.models import load_model
from flask import jsonify 
from flask import request
from flask import Flask
from PIL import Image
import numpy as np
import datetime
import random
import time
import json
import h5py
import uuid
import io
import os

# Flask konfigurieren
app = Flask(__name__)
app.config["CORS_HEADERS"] = "Content-Type"
cors = CORS(app)

# Modelle laden
model = load_model("model_classification")
detector = load_model("bird_detector")

# Route, um zu prüfen, Flask richtig konfiguriert wurde.
# Diese Route ist irrelevant für die App und dient lediglich einem kurzen Test.
@app.route("/hello_world", methods=["GET"])
@cross_origin()
def get():
    return "Hello World!"

# Diese Route dient der Klassifikation eines Bildes.
@app.route("/classify", methods=["POST", "GET"])
@cross_origin()
def classify():
    with open('log.txt', 'a') as the_file:
       the_file.write(datetime.datetime.today().strftime("%d.%m.%Y, %H:%M")+'\n')
    try:
        # Laden des Bildes und Skalierung zu 224x224
        data = request.data

        # Bild zu RGB konvertieren
        img = Image.open(io.BytesIO(data)).convert("RGB")
        img = np.array(img)
        img = cv2.resize(img, (224, 224))

        # Diesen Schritt kann man eigentlich weglassen, da die preprocess_input-Funktion keine Funktion besitzt. Der Vollständigkeithalber wird sie aber
        # dennoch verwendet.
        img = preprocess_input(img)

        # Um eine Vorhersage zu treffen, muss das Bild zu einem 4D-Array konvertiert werden.
        img = img.reshape(1, 224, 224, 3)

        predictions = model.predict(img)[0]

        # Schauen, ob auf Bild überhaupt ein Vogel abgebildet ist
        detector_prediction = detector.predict(img)[0]

        if detector_prediction[0] < detector_prediction[1]:
            isBird = False
        else:
            isBird = True

        # Laden der Namen der Labels
        species = []
        with open("labels.txt") as txt_file:
            for label in txt_file:
                species.append(label.strip("\n"))

        # Das Neuronale Netz erzeugt 307 Output Werte. Der erste der 307 Werte entspricht dem Label, was bei alphabetischer Sortierung der Artennamen zuerst kommt (Accipiter gentilis).
        species = sorted(species)
        print(species[0], len(species), len(predictions))

        # Ermittlung der Top 5 Predictions
        highest_classes = (-predictions).argsort()[:5]

        # Wahrscheinlichkeiten der Top 5
        pred_sum = 1
        pred_1 = predictions[highest_classes[0]] / pred_sum
        pred_2 = predictions[highest_classes[1]] / pred_sum
        pred_3 = predictions[highest_classes[2]] / pred_sum
        pred_4 = predictions[highest_classes[3]] / pred_sum
        pred_5 = predictions[highest_classes[4]] / pred_sum

        data = {
            "top1": str(species[highest_classes[0]]),
            "top2": str(species[highest_classes[1]]),
            "top3": str(species[highest_classes[2]]),
            "top4": str(species[highest_classes[3]]),
            "top5": str(species[highest_classes[4]]),
            "prob1": "{0:.0%}".format(pred_1),
            "prob2": "{0:.0%}".format(pred_2),
            "prob3": "{0:.0%}".format(pred_3),
            "prob4": "{0:.0%}".format(pred_4),
            "prob5": "{0:.0%}".format(pred_5),
            "isBird": isBird,
        }
        print(data)
        response = app.response_class(response=json.dumps(
            data), status=200, mimetype="application/json")
        return response
    except:
        # Sollte etwas nicht funktioniert haben bei dem Request, soll 404 als HTTP Error-Code zurückgegeben werden
        print("Error")
        return abort(404)


bird_of_the_day = None

# Schließlich wird noch das nebensächliche Feature "Vogel des Tages" implementiert.
def set_bird_of_the_day():

    # Laden der Namen der Labels
    species = []
    with open("labels.txt") as txt_file:
        for label in txt_file:
            species.append(label.strip("\n"))

    global bird_of_the_day
    bird_of_the_day = [random.choice(species), datetime.datetime.today().strftime("%d.%m.%Y, %H:%M")]
    print(bird_of_the_day)
    return bird_of_the_day

bird_of_the_day = set_bird_of_the_day()

scheduler = BackgroundScheduler()
scheduler.add_job(func=set_bird_of_the_day, trigger="interval", days=1)
scheduler.start()

@app.route("/get_vdt", methods=["GET"])
@cross_origin()
def get_vdt():
    return jsonify({"timestamp": bird_of_the_day[1], "species_name": bird_of_the_day[0]})

@app.route("/datenspende", methods=["POST"])
@cross_origin()
def datenspende():
    data = request.data

    # Bild zu RGB konvertieren
    img = Image.open(io.BytesIO(data)).convert("RGB")
    img = np.array(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (224, 224))
    # Datenspende
    cv2.imwrite("Datenspende/"+str(uuid.uuid4())[:10]+".jpg", img)
    response = app.response_class(status=200, mimetype="application/json")
    return response


@app.route("/send_error_report", methods=["POST"])
@cross_origin()
def sendErrorReport():
    data = json.loads(request.data)
    with open("Fehlerreports/"+str(uuid.uuid4())[:10]+".txt", "w") as text_file:
        text_file.write("Beschreibung: "+data["problemText"]+"\nE-Mail: "+data["emailText"]+"\nZeitinformationen: "+datetime.datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
    print(data)
    return {"response": "Success!"}


if __name__ == "__main__":
    app.run(host="0.0.0.0", threaded=False, port=int(os.getenv('PORT', 4444)))